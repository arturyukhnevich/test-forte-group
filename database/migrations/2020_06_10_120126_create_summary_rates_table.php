<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSummaryRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('summary_rates', static function (Blueprint $table) {
            $table->id();
            $table->string('country');
            $table->string('state')->nullable();
            $table->float('minimum_rate');
            $table->float('average_rate');
            $table->timestamp('date_add')->nullable();
            $table->timestamp('date_upd')->nullable();

            $table->unique(['country', 'state'], 'summary_rates_country_and_state_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('summary_rates');
    }
}
