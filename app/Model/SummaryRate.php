<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Yadakhov\InsertOnDuplicateKey;

/**
 * Class SummaryRate
 *
 * @package App\Model
 */
class SummaryRate extends Model
{
    use InsertOnDuplicateKey;

    const CREATED_AT = 'date_add';
    const UPDATED_AT = 'date_upd';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'country', 'state', 'minimum_rate', 'average_rate',
    ];
}
