<?php

namespace App\Console\Commands;

use App\Model\SummaryRate;
use Carbon\Carbon;
use Illuminate\Console\Command;
use TaxJar\Client;

class ImportTax extends Command
{
    /**
     * @var string
     */
    protected $signature = 'tax:import';

    /**
     * @var string
     */
    protected $description = 'Import summarize tax rates for all regions';

    private $client;

    /**
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->client = Client::withApiKey(getenv('TAXJAR_API_KEY'));
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $summarizedRates = $this->client->summaryRates();

        $data = [];
        foreach ($summarizedRates as $summarizedRate) {
            $rate['country'] = $summarizedRate->country_code ?? '';
            $rate['state'] = $summarizedRate->region_code ?? '';
            $rate['minimum_rate'] = $summarizedRate->minimum_rate->rate ?? null;
            $rate['average_rate'] = $summarizedRate->minimum_rate->rate ?? null;
            $rate['date_add'] = Carbon::now();
            $rate['date_upd'] = Carbon::now();

            $data[] = $rate;
        }

        SummaryRate::insertOnDuplicateKey($data, ['minimum_rate', 'average_rate', 'date_upd']);
    }
}
