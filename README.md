### Requirement  ###
* PHP 7.3+
* MySQL 5.7+
* Composer

### Run ###
1. `php composer install`
2. `php artisan migrate`
3. `cp .env.exapmple .env`
4. `php artisan key:generate`
5. In file `.env` configure the database connection
5. In file `.env` set TAXJAR_API_KEY
6. `php artisan tax:import`
